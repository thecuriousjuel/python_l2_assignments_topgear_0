class Mymath:
    def __init__(self, num1, num2):
        self.num1 = num1
        self.num2 = num2

    def sqroot(self):
        return self.num1 ** 0.5

    def addition(self):
        return self.num1 + self.num2

    def subtraction(self):
        return self.num1 - self.num2

    def multiplication(self):
        return self.num1 * self.num2

    def division(self):
        try:
            return self.num1 / self.num2
        except ZeroDivisionError:
            return 'Cannot Divide by Zero'


get_num1 = float(input('Enter a number -> '))
get_num2 = float(input('Enter another number -> '))

obj = Mymath(get_num1, get_num2)

print(f'Square Root of {get_num1} is {obj.sqroot()}')
print('Addition result is : ', obj.addition())
print('Subtraction result is : ', obj.subtraction())
print('Multiplication result is : ', obj.multiplication())
print('Division result is : ', obj.division())

