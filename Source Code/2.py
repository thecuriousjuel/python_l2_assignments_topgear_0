import re

string = 'From abc.xyz@pqr.com Mon Dec 29 01:12:15 2016'

email_pattern = '[a-zA-Z0-90.]+@[a-zA-Z0-9.]+(com)'
domain_pattern = '[a-zA-Z0-9.]+(com)'
time_pattern = '[0-9]+:[0-9]+:[0-9]+'

email = re.search(email_pattern, string).group()
print('Email Id : ',email)

domain = re.search(domain_pattern, string).group()
print('Domain Name : ', domain)

time = re.search(time_pattern, string).group()
print('Time : ', time)
