from math import pi

class Circle:

    def area(radius):
        return pi*(radius**2)
    
    def circumference(radius):
        return 2 * pi * radius

    def __str__(radius):
        return f'Radius is : {radius} units'

    
c1 = float(input('Enter the radius -> '))
print(Circle.__str__(c1))
print('Area : {0:.2f} unit square.'.format(Circle.area(c1)))
print('Circumference : {0:.2f} units.'.format(Circle.circumference(c1)))

